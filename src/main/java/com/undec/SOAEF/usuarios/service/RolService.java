package com.undec.SOAEF.usuarios.service;

import com.undec.SOAEF.usuarios.entity.Rol;
import com.undec.SOAEF.usuarios.enums.RolNombre;
import com.undec.SOAEF.usuarios.repository.RolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public class RolService {

    @Autowired
    RolRepository rolRepository;

    public Optional<Rol> findByNombre(RolNombre rolNombre){
        return rolRepository.findByRolNombre(rolNombre);
    }
}
