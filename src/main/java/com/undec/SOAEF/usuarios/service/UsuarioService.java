package com.undec.SOAEF.usuarios.service;

import com.undec.SOAEF.usuarios.entity.Usuario;
import com.undec.SOAEF.usuarios.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UsuarioService {

    @Autowired
    UsuarioRepository usuarioRepository;

    public List<Usuario> listUser(){
        return usuarioRepository.findAll();
    }
    public Optional<Usuario> getUserById(int id){
        return usuarioRepository.findById(id);
    }
    public void deleteUser(int id){
        usuarioRepository.deleteById(id);
    }

    public Optional<Usuario> getByNombreUsuario(String nombreUsuario) {
        return usuarioRepository.findByNombreUsuario(nombreUsuario);
    }

    public boolean existById(int id){
        return usuarioRepository.existsById(id);
    }

    public boolean existsByNombreUsuario(String nombreUsuario) {
        return usuarioRepository.existsByNombreUsuario(nombreUsuario);
    }

    public boolean existeByEmail(String email){
        return usuarioRepository.existsByEmail(email);
    }

    public void save(Usuario usuario){
        usuarioRepository.save(usuario);
    }
}