package com.undec.SOAEF.usuarios.entity;


import com.undec.SOAEF.usuarios.enums.RolNombre;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
@Entity
public class Rol {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private RolNombre rolNombre;

    public Rol(){}

    public Rol(RolNombre rolNombre) {
        this.rolNombre = rolNombre;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public @NotNull RolNombre getRolNombre() {
        return rolNombre;
    }

    public void setRolNombre(@NotNull RolNombre rolNombre) {
        this.rolNombre = rolNombre;
    }
}
