package com.undec.SOAEF.usuarios.controller;
import com.undec.SOAEF.security.utiles.Mensaje;
import com.undec.SOAEF.usuarios.dto.ActualizarUsuario;
import com.undec.SOAEF.usuarios.dto.NuevoUsuario;
import com.undec.SOAEF.usuarios.entity.Rol;
import com.undec.SOAEF.usuarios.entity.Usuario;
import com.undec.SOAEF.usuarios.enums.RolNombre;
import com.undec.SOAEF.usuarios.service.RolService;
import com.undec.SOAEF.usuarios.service.UsuarioService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/usuario")
@CrossOrigin(origins = "http://localhost:4200")
public class UsuarioController {

    @Autowired
    UsuarioService usuarioService;
    @Autowired
    RolService rolService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @GetMapping("/lista")
    public ResponseEntity<List<Usuario>> listarUsuarios(){
        List<Usuario> usuarios = usuarioService.listUser();
        return new ResponseEntity(usuarios, HttpStatus.OK);
    }

    @GetMapping("/detalle/{id}")
    public ResponseEntity<Usuario> buscarUsuario(@PathVariable("id") int id){
        if(!usuarioService.existById(id))
            return new ResponseEntity(new Mensaje("El usuario no existe"), HttpStatus.NOT_FOUND);
        Usuario usuario = usuarioService.getUserById(id).get();
        return new ResponseEntity(usuario, HttpStatus.OK);
    }

    @PostMapping("/registrar")
    public ResponseEntity<?> registrarUsuario(@Valid @RequestBody NuevoUsuario nuevoUsuario, BindingResult bindingResult){
        if(bindingResult.hasErrors())
            return new ResponseEntity(new Mensaje("campos mal puestos o email invalido"), HttpStatus.BAD_REQUEST);
        if(usuarioService.existsByNombreUsuario(nuevoUsuario.getNombreUsuario()))
            return new ResponseEntity(new Mensaje("el nombre ya existe"), HttpStatus.BAD_REQUEST);
        if(usuarioService.existeByEmail(nuevoUsuario.getEmail()))
            return new ResponseEntity(new Mensaje("el email ya existe"), HttpStatus.BAD_REQUEST);
        Usuario usuario = new Usuario(nuevoUsuario.getNombre(), nuevoUsuario.getNombreUsuario(), nuevoUsuario.getEmail(), passwordEncoder.encode(nuevoUsuario.getPassword()));
        Set<Rol> roles = new HashSet<>();
        nuevoUsuario.getRoles().forEach(role -> {
            switch (role.toLowerCase()) {
                case "admin":
                    roles.add(rolService.findByNombre(RolNombre.ROLE_ADMIN).orElseThrow(() -> new RuntimeException("Error: Role no encontrado.")));
                    break;
                default:
                    roles.add(rolService.findByNombre(RolNombre.ROLE_USER).orElseThrow(() -> new RuntimeException("Error: Role no encontrado.")));
            }
        });
        usuario.setRoles(roles);
        usuarioService.save(usuario);
        return new ResponseEntity("Usuario registrado correctamente",HttpStatus.CREATED);
    }

    @PutMapping("/actualizar/{id}")
    public ResponseEntity<?> actualizarUsuario(@PathVariable("id") Integer id, @Valid @RequestBody ActualizarUsuario actualizarUsuario, BindingResult bindingResult) {
        if (bindingResult.hasErrors())
            return new ResponseEntity<>(new Mensaje("Campos mal puestos o email inválido"), HttpStatus.BAD_REQUEST);

        // Verifica si el usuario existe
        if (!usuarioService.existById(id))
            return new ResponseEntity<>(new Mensaje("El usuario no existe"), HttpStatus.NOT_FOUND);

        // Verifica si el nombre de usuario o email ya existen para otro usuario
        if (usuarioService.existsByNombreUsuario(actualizarUsuario.getNombreUsuario()) &&
                !usuarioService.getUserById(id).get().getNombreUsuario().equals(actualizarUsuario.getNombreUsuario()))
            return new ResponseEntity<>(new Mensaje("El nombre de usuario ya existe"), HttpStatus.BAD_REQUEST);

        if (usuarioService.existeByEmail(actualizarUsuario.getEmail()) &&
                !usuarioService.getUserById(id).get().getEmail().equals(actualizarUsuario.getEmail()))
            return new ResponseEntity<>(new Mensaje("El email ya existe"), HttpStatus.BAD_REQUEST);

        // Obtener el usuario existente
        Usuario usuario = usuarioService.getUserById(id).get();
        usuario.setNombre(actualizarUsuario.getNombre());
        usuario.setNombreUsuario(actualizarUsuario.getNombreUsuario());
        usuario.setEmail(actualizarUsuario.getEmail());

        // Actualizar roles
        Set<Rol> roles = new HashSet<>();
        actualizarUsuario.getRoles().forEach(role -> {
            switch (role.toLowerCase()) {
                case "admin":
                    roles.add(rolService.findByNombre(RolNombre.ROLE_ADMIN).orElseThrow(() -> new RuntimeException("Error: Rol no encontrado.")));
                    break;
                default:
                    roles.add(rolService.findByNombre(RolNombre.ROLE_USER).orElseThrow(() -> new RuntimeException("Error: Rol no encontrado.")));
            }
        });
        usuario.setRoles(roles);

        // Guardar los cambios
        usuarioService.save(usuario);

        return new ResponseEntity<>(new Mensaje("Usuario actualizado correctamente"), HttpStatus.OK);
    }


    @DeleteMapping("/eliminar/{id}")
    public ResponseEntity<?> delete(@PathVariable("id")int id){
        if(!usuarioService.existById(id))
            return new ResponseEntity(new Mensaje("no existe"), HttpStatus.NOT_FOUND);
        usuarioService.deleteUser(id);
        return new ResponseEntity(new Mensaje("usuario eliminado"), HttpStatus.OK);
    }
}
