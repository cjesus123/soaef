package com.undec.SOAEF.usuarios.repository;


import com.undec.SOAEF.usuarios.entity.Rol;
import com.undec.SOAEF.usuarios.enums.RolNombre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RolRepository extends JpaRepository<Rol,Integer> {
    Optional<Rol> findByRolNombre(RolNombre rolNombre);
}
