package com.undec.SOAEF.usuarios.utiles;

import com.undec.SOAEF.usuarios.entity.Rol;
import com.undec.SOAEF.usuarios.entity.Usuario;
import com.undec.SOAEF.usuarios.enums.RolNombre;
import com.undec.SOAEF.usuarios.service.RolService;
import com.undec.SOAEF.usuarios.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.HashSet;
import java.util.Set;

public class CrearAdminApplication {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    public static void main(String[] args) {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        System.out.println(passwordEncoder.encode("ADMIN"));
    }
}
